import {Component} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';

export interface PeriodicElement {
  companyname: string;
  experience: string;
  location: string;
  skill: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {experience: '1-2' ,  companyname: 'apple',        location: 'bangalore' , skill: 'Ajax'},
  {experience: '6-7',  companyname: 'HP',           location: 'bangalore' , skill: 'cassandra'},
  {experience: '0-1',  companyname: 'Wipro',        location: 'USA' ,       skill: 'Blockchain'},
  {experience: '0-3',  companyname: 'Exultsoft',    location: 'Mysore' ,    skill: 'salesforce'},
  {experience: '1-3',  companyname: 'TechMahindra', location: 'Mysore' ,    skill: 'Big Data'},
  {experience: '3-8',  companyname: 'Google',       location: 'USA' ,       skill: 'Network'},
  {experience: '1-2',  companyname: 'amazon',       location: 'Delhi' ,     skill: 'Ajax'},
  {experience: '2-3',  companyname: 'flipart',      location: 'Delhi' ,     skill: 'Visual Basic'},
  {experience: '1-2',  companyname: 'a2pro',        location: 'Bangalore' , skill: 'Blockchain'},
  {experience: '0',  companyname: 'ethnus',       location: 'Mumbai' ,    skill: 'cassandra'},



];

/**
 * @title Table with filtering
 */
@Component({
  // tslint:disable-next-line: component-selector
  selector: 'table-filtering-example',
  styleUrls: ['table-filtering-example.css'],
  templateUrl: 'table-filtering-example.html',
})
// tslint:disable-next-line: component-class-suffix
export class TableFilteringExample {
  displayedColumns: string[] = ['experience', 'companyname', 'location', 'skill'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
